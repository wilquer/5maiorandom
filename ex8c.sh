#!/bin/bash

#Comando teste= testa Números,Strings,Arquivos,Variáveis
# pra não usar o TESTE direto

# Pra testar arquivos
#-------------------------
# [ -E arq ]
# [ -d arq ]
# [ -f arq ]
# [ -x arq ]
# [ -s arq ]

# Pra testar Números
#-----------------------
# [ -eq 1 ]
# [ -ne 2 ]
# [ -gt 2 ]
# [ -ge 2 ]
# [ -lt 2 ]
# [ -le 2 ]

#Pra testar Strigs
#----------------------
# [ "A" =="B" ]
# [ "A" != "B" ]

# Pra testar Variáveis
#----------------------
# [ -z $x ]
# [ -n $x ]

# -------------------------------------------------------
