#!/bin/bash

#Comando teste= testa Números,Strings,Arquivos,Variáveis


# Pra testar arquivos
#-------------------------
# test -E arq
# test -d arq
# test -f arq
# test -x arq
# test -s arq

# Pra testar Números
#-----------------------
#test 1 -eq 1
#test 1 -ne 2
#test 1 -gt 2
#test 1 -ge 2
#test 1 -lt 2
#test 1 -le 2

#Pra testar Strigs
#----------------------
# test "A" =="B"
# "A" != "B"

# Pra testar Variáveis
#----------------------
# test -z $x
# test -n $x

